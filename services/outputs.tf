output "ec2_id" {
    description = "ID of EC2 instance"
    value = aws_instance.my_ec2.id
}

output "subnet_id" {
  description = "ID of subnet"
  value = data.terraform_remote_state.network.outputs.subnet_id
}

