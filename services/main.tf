
data "terraform_remote_state" "network" {
    backend = "s3"

    config = {
        bucket  = "myphi-practice-terreform-state-2811"
        key     = "network/terraform.tfstate"
        region  = "us-west-2"
    }
}

resource "aws_instance" "my_ec2" {
    ami = "ami-08e4eaf54ff5ee95e"
    instance_type = "t2.micro"
    subnet_id = data.terraform_remote_state.network.outputs.subnet_id
}