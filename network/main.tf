terraform {
# # Release this block after run init command first time

  # backend "s3" {
  #   # Replace with bucket name
  #   bucket  = "myphi-practice-terreform-state-2811"
  #   key     = "network/terraform.tfstate"
  #   region  = "us-west-2"

  #   # DynamoDB
  #   dynamodb_table = "terraform-up-and-running-locks"
  #   encrypt = true
  # }
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "myphi-practice-terreform-state-2811"

  #Prevent accidental deletion of this S3 bucket
  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_s3_bucket_versioning" "enabled" {
  bucket = aws_s3_bucket.terraform_state.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "default" {
  bucket = aws_s3_bucket.terraform_state.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "public_access" {
  bucket                  = aws_s3_bucket.terraform_state.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "terraform-up-and-running-locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}

resource "aws_vpc" "phi_vpc" {
  cidr_block = "172.16.0.0/16"

  tags = {
    Name = "vpc-2811"
  }
}

resource "aws_subnet" "phi_subnet" {
  vpc_id            = aws_vpc.phi_vpc.id
  cidr_block        = "172.16.10.0/24"
  availability_zone = "us-west-2a"

  tags = {
    Name = "subnet-2811"
  }
}

resource "aws_security_group" "vpc_sg" {
  name = "phi-terraform-instance"
  vpc_id = aws_vpc.phi_vpc.id
  
}