output "vpc_id" {
  description = "ID of VPC"
  value = aws_vpc.phi_vpc.id
}

output "subnet_id" {
  description = "ID of subnet"
  value = aws_subnet.phi_subnet.id
}